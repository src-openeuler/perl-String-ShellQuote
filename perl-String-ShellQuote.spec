Name:           perl-String-ShellQuote
Version:        1.04
Release:        27
Summary:        Perl module for quoting strings for passing through the shell
License:        (GPL-1.0-or-later OR Artistic-1.0-Perl) and GPLv2+
URL:            https://metacpan.org/release/String-ShellQuote
Source0:        https://cpan.metacpan.org/authors/id/R/RO/ROSCH/String-ShellQuote-%{version}.tar.gz
BuildArch:      noarch
BuildRequires:  perl-interpreter perl-generators perl(ExtUtils::MakeMaker) >= 6.76
BuildRequires:  perl(Carp) perl(Exporter) perl(strict) perl(vars)
Requires:       perl(Carp) perl(Getopt::Long)

%description
This package contains the String::ShellQuote module, plus a command-line
interface to it.  It contains some functions which are useful for quoting
strings which are going to pass through the shell or a shell-like object.  It
is useful for doing robust tool programming, particularly when dealing with
files whose names contain white space or shell globbing characters.

%package_help

%prep
%autosetup -n String-ShellQuote-%{version}

%build
perl Makefile.PL INSTALLDIRS=vendor NO_PACKLIST=1
%make_build

%install
make pure_install DESTDIR=$RPM_BUILD_ROOT
%{_fixperms} $RPM_BUILD_ROOT/*

%check
make test

%files
%{_bindir}/shell-quote
%{perl_vendorlib}/String

%files help
%doc Changes README
%{_mandir}/man1/*
%{_mandir}/man3/*

%changelog
* Sun Jan 19 2025 Funda Wang <fundawang@yeah.net> - 1.04-27
- drop useless perl(:MODULE_COMPAT) requirement

* Tue Nov 19 2019 mengxian <mengxian@huawei.com> - 1.04-26
- Package init
